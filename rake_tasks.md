# Rake tasks

## General

* `gitlab-rake gitlab:env:info`
* `gitlab-rake gitlab:check`

## Database

* `gitlab-rake db:migrate:status` (check migrations)
* `gitlab-rake db:migrate` (run migrations)
* `gitlab-rake gitlab:db:reindex` (reindex)

### Geo databases

* `gitlab-rake db:migrate:status:geo` (check migrations)
* `gitlab-rake db:migrate:geo` (run migrations)

## Storage

* `gitlab-rake gitlab:git:fsck`
* `gitlab-rake gitlab:artifacts:check VERBOSE=1`
* `gitlab-rake gitlab:lfs:check VERBOSE=1`
* `gitlab-rake gitlab:uploads:check VERBOSE=1`
* `gitlab-rake gitlab:doctor:secrets VERBOSE=1`

## Geo

* `gitlab-rake gitlab:geo:check`
* `gitlab-rake geo:status`
