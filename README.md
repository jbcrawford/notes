# Notes

This repo is full of handy notes for administering and troubleshooting GitLab.
Mostly just things I can never remember and end up searching our docs for
several times a day. It may not be entirely up to date, so if anything is weird
consult the actual GitLab docs.

* [Rake tasks](rake_tasks.md)
* [Geo](geo_troubleshooting.md)
