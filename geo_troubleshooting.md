# Geo troubleshooting

todo: some of the usefulness of this file could be replaced by reorganizing the
official Geo docs, but, well, that's pretty daunting.

## Failed sync

### Projects

Find failures:

* `Geo::ProjectRegistry.verification_failed('repository')`
* `Geo::ProjectRegistry.sync_failed('repository')`
* `Geo::ProjectRegistry.repositories_checksummed_pending_verification` (this finds repos stuck in an in-between state)

Manually synchronize, synchronously:

```
project = Project.find_by_full_path('<group/project>')
Geo::RepositorySyncService.new(project).execute
```

### Other repos

These can be a pain to figure out problems with. I'm not totally sure how to
sync these synchronously to see errors easily. Digging through the registry for
problems can be helpful but I've just ended up reading source code to figure
out how.

### Files

Classes:

*    Ci::JobArtifact / Geo::JobArtifactRegistry
*    Ci::PipelineArtifact / Geo::PipelineArtifactRegistry
*    Ci::SecureFile / Geo::SecureFileRegistry
*    LfsObject / Geo::LfsObjectRegistry
*    MergeRequestDiff / Geo::MergeRequestDiffRegistry
*    Packages::PackageFile / Geo::PackageFileRegistry
*    PagesDeployment / Geo::PagesDeploymentRegistry
*    Terraform::StateVersion / Geo::StateVersionRegistry
*    Upload / Geo::UploadRegistry

Not all of these classes have the same features, some of the most useful
methods only exist for the more common types like JobArtifact.

Find items that failed sync:

`Geo::JobArtifactRegistry.failed`

Much of the time it's even more useful to run the artifact integrity check on
the primary. The replication for files doesn't have that many reasons it fails,
the most common in practice is the file not being available on the primary to
begin with.
