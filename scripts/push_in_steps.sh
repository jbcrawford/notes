#!/bin/bash
# This shell script comes from https://stackoverflow.com/questions/15125862/github-remote-push-pack-size-exceeded
# It checks the latest ref on branch on the server and then produces a git log of the difference
# Then it pushes BATCH_SIZE commits from that log at a time
# Useful when a client's GitLab or whatever other Git server has a low pack size limit set and you
# want to push a whole big repo without having to wait for them to change it.

# Usage: drop in the repo root and run. You may need to turn down BATCH_SIZE dependong on how
# hard you're hitting limits. Retry with smaller BATCH_SIZE until the repo makes it.

# Adjust the following variables as necessary
REMOTE=origin
BRANCH=$(git rev-parse --abbrev-ref HEAD)
BATCH_SIZE=50

# check if the branch exists on the remote
if git show-ref --quiet --verify refs/remotes/$REMOTE/$BRANCH; then
    # if so, only push the commits that are not on the remote already
    range=$REMOTE/$BRANCH..HEAD
else
    # else push all the commits
    range=HEAD
fi

# count the number of commits to push
n=$(git log --first-parent --format=format:x $range | wc -l)

# push each batch
for i in $(seq $n -$BATCH_SIZE 1); do
    # get the hash of the commit to push
    h=$(git log --first-parent --reverse --format=format:%H --skip $i -n1)
    echo "Pushing $h..."
    git push $REMOTE ${h}:refs/heads/$BRANCH
done

# push the final partial batch
git push $REMOTE HEAD:refs/heads/$BRANCH